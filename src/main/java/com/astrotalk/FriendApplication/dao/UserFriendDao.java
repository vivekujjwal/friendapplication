package com.astrotalk.FriendApplication.dao;

import com.astrotalk.FriendApplication.entity.User;

import java.util.List;
import java.util.Set;

import org.springframework.data.repository.CrudRepository;


public interface UserFriendDao extends CrudRepository<User,Long>{
	
	User findByEmail(String email);
	List<User> findAll();
	 User findById(int id);
	

}
