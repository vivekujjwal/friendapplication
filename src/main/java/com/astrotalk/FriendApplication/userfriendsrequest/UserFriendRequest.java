package com.astrotalk.FriendApplication.userfriendsrequest;

import java.util.List;

public class UserFriendRequest {
	private List<String> userfriends;

	public List<String> getUserfriends() {
		return userfriends;
	}

	public void setUserfriends(List<String> userfriends) {
		this.userfriends = userfriends;
	}

	

}
