package com.astrotalk.FriendApplication.service;

import com.astrotalk.FriendApplication.entity.User;
import com.astrotalk.FriendApplication.userfriendsrequest.*;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;


public interface FriendsService {
	ResponseEntity<Map<String, Object>> addUserFriends(UserFriendRequest userFriendsRequestEntity);
	ResponseEntity<Map<String, Object>> getUserFriendsList(UserFriendListRequest userFriendListRequest);
	
	public ResponseEntity<Map<String, Object>> deleteFromFriendList(@RequestBody UserFriendDelete userFriendDelete);
	public ResponseEntity<Map<String, Object>> createUser(User user);
	public ResponseEntity<Map<String, Object>> findConnections(ConnectionRequestK connectionRequestK); 

	 

}
