package com.astrotalk.FriendApplication.serviceImpl;

import com.astrotalk.FriendApplication.entity.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Queue;
import java.util.Vector;
import java.util.stream.Collectors;
import java.util.regex.Pattern;


import com.astrotalk.FriendApplication.userfriendsrequest.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.astrotalk.FriendApplication.dao.UserFriendDao;
import com.astrotalk.FriendApplication.service.FriendsService;


@Service
public class FriendsServiceImpl implements FriendsService {
	
@Autowired
private UserFriendDao userFriendDao;

private User Register(String email) {

	Map<String, Object> result = new HashMap<String, Object>();
	User userPresent = this.userFriendDao.findByEmail(email);
	if (userPresent == null) {
		userPresent = new User();
		userPresent.setEmail(email);
		return this.userFriendDao.save(userPresent);
	} else {
		return userPresent;
	}
}
@Override
public ResponseEntity<Map<String, Object>> createUser(User user) {
	Pattern pattern = Pattern.compile("^(.+)@(.+)$");
	Map<String, Object> result = new HashMap<String, Object>();
	String email2 = user.getEmail();
	 email2 = email2.toLowerCase();
	 if(email2 == null)
	 {
		 result.put("Error : ", "Invalid request");
			return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
		
	 }
	 
	 else if(!pattern.matcher(email2).matches())
	 {
		 result.put("Info : ", "Email Id is invalid");
			return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
	 }
	 
	 User userPresent = this.userFriendDao.findByEmail(email2);
		if (userPresent == null) {
			userPresent = new User();
			userPresent.setEmail(email2);
			this.userFriendDao.save(userPresent);
			result.put("Success", true);
			result.put("email added", email2);
		}
		else
		{
			result.put("Error : ", "User with email ID already present");
			result.put("user not added",email2);
		}
	
		
		
		return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
	

	
}

@Override
	public ResponseEntity<Map<String, Object>> addUserFriends(UserFriendRequest userFriendsRequest)
	{
 

		Map<String, Object> result = new HashMap<String, Object>();

		if (userFriendsRequest == null) {
			result.put("Error : ", "Invalid request");
			return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
		}

		if (CollectionUtils.isEmpty(userFriendsRequest.getUserfriends())) {
			result.put("Error : ", "Friend list cannot be empty");
			return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
		}
		if (userFriendsRequest.getUserfriends().size() != 2) {
			result.put("Info : ", "Please provide 2 emails to make them friends");
			return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
		}

		String email1 = userFriendsRequest.getUserfriends().get(0);
		String email2 = userFriendsRequest.getUserfriends().get(1);

		email1.toLowerCase();
		email2.toLowerCase();
		if (email1.equals(email2)) {
			result.put("Info : ", "Users are same cannot add friends");
			return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
		}

		User user1 = null;
		User user2 = null;
		user1 = this.Register(email1);
		user2 = this.Register(email2);

		if (user1.getUserFriends().contains(user2)) {
			result.put("Info : ", "Both are already friends :friend not added");
			return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
		}

		

		user1.addUserFriends(user2);
		this.userFriendDao.save(user1);
		

		user2.addUserFriends(user1);
		this.userFriendDao.save(user2);
		result.put("Info : ", "successfully added friends");
		result.put("Success", true);

		return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
	
}
@Override
	public ResponseEntity<Map<String, Object>> getUserFriendsList(UserFriendListRequest userFriendListRequest) {

		Map<String, Object> result = new HashMap<String, Object>();

		if (userFriendListRequest == null) {
			result.put("Error : ", "Invalid request");
			return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
		}

		User user = this.userFriendDao.findByEmail(userFriendListRequest.getEmail());
		List<String> friendList = user.getUserFriends().stream().map(User::getEmail).collect(Collectors.toList());
		List<User> Allusers = new ArrayList<User>();
		  Allusers = this.userFriendDao.findAll();
		result.put("success", true);
		result.put("friends", friendList);
		result.put("count", friendList.size());
		

		return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);

	}

@Override
public ResponseEntity<Map<String, Object>> deleteFromFriendList(UserFriendDelete userFriendDelete) {

	Map<String, Object> result = new HashMap<String, Object>();

	if (userFriendDelete == null) {
		result.put("Error : ", "Invalid request");
		return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
	}

	User user = this.userFriendDao.findByEmail(userFriendDelete.getEmail1());
	List<String> friendList = user.getUserFriends().stream().map(User::getEmail).collect(Collectors.toList());
     if(!friendList.contains(userFriendDelete.getEmail2().toLowerCase()))
    		{
    	result.put("Info : ", "User is not a friend");
    	return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
    		}
     User user2 = null;
     User user1 = null;
     user1 = this.Register(userFriendDelete.getEmail1());
     user2 = this.Register(userFriendDelete.getEmail2());
     
     user1.removeUserFriends(user2);
     
    
		List<String> friendList2 = user1.getUserFriends().stream().map(User::getEmail).collect(Collectors.toList());
		
	this.userFriendDao.save(user1);
	user2.removeUserFriends(user1);
	this.userFriendDao.save(user2);
	result.put("success", true);
	result.put("friends", friendList2);
	result.put("count", friendList2.size());
	 
	  

	return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);

}
@Override
public ResponseEntity<Map<String, Object>> findConnections(ConnectionRequestK connectionRequestK) {
	
	int level = connectionRequestK.getK();
	
	int startingedge = this.userFriendDao.findByEmail(connectionRequestK.getEmail()).getId();
	  List<User> Allusers = new ArrayList<User>();
	  List<String> friendList = new ArrayList<String>();
	  Allusers = this.userFriendDao.findAll();
	  int V = Allusers.size();
	  
	  ArrayList<ArrayList<Integer> > adj  = new ArrayList<ArrayList<Integer> >(V);
	  Vector<Vector<Integer>> graph=new Vector<Vector<Integer>>(); 
	   
	  for(int i = 0; i < V + 1; i++) 
	  { graph.add(new Vector<Integer>()); 
	  }
	  
	  for(int i=0;i<V;i++)
	  {
		  friendList = Allusers.get(i).getUserFriends().stream().map(User::getEmail).collect(Collectors.toList());
		  int W = friendList.size();
			  for(int j=0;j<W;j++)
			  {
				 User s= this.userFriendDao.findByEmail(friendList.get(j));
				 graph.get((Allusers.get(i).getId())-1).add((s.getId())-1);
			  }
		  
	  }

	  ArrayList<Integer> res = getLevels(graph, V, (startingedge-1),level);
	  ArrayList<String> connectionsAtK = new ArrayList<String>();
	  for (int z=0;z<res.size();z++)
	  {
		  int w = res.get(z);
		  User userResponse =this.userFriendDao.findById(w);
		  connectionsAtK.add(userResponse.getEmail());
	  }
	  Map<String, Object> result = new HashMap<String, Object>();

	  result.put("success", true);
	  result.put("Level", level);
		result.put("All friends at level",connectionsAtK );
		
		 
		  

		return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
}
static ArrayList<Integer> getLevels(Vector<Vector<Integer> > graph, 
        int V, int x,int kthlev) 
{ 
	int level[] = new int[V];  
	ArrayList<Integer> conAtK = new ArrayList<Integer>();
    boolean marked[] = new boolean[V]; 
    Queue<Integer> que = new LinkedList<Integer>();
    que.add(x); 
    marked[x] = true; 
    while (que.size() > 0)  
    {  
   
        x = que.peek();  
  
        que.remove();  
   
        for (int i = 0; i < graph.get(x).size(); i++)  
        {  
            int b = graph.get(x).get(i);  

            if (!marked[b]) 
            {  
   
                que.add(b);  
  
                level[b] = level[x] + 1;  
                marked[b] = true;  
            }  
        }  
    
} 
    for (int i = 0; i < V; i++) 
    {
    	if(level[i] == kthlev)
    	{
    		conAtK.add(i+1);
    	}
    }
    
    return conAtK;
}




}
