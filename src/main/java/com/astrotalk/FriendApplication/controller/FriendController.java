package com.astrotalk.FriendApplication.controller;
import com.astrotalk.FriendApplication.entity.*;
import com.astrotalk.FriendApplication.dao.*;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Map;

import com.astrotalk.FriendApplication.userfriendsrequest.*;
import com.astrotalk.FriendApplication.userfriendsrequest.UserAddRequest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import com.astrotalk.FriendApplication.service.FriendsService;

@RestController
public class FriendController {
	
	
@Autowired
private FriendsService friendsService;

@Autowired
private UserFriendDao userFriendDao;

@RequestMapping(value = "/addUser" , method =RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> createUser(@RequestBody User user)
	{
		
	    return this.friendsService.createUser(user);
	 }

@RequestMapping(value = "/addFriends", method = RequestMethod.POST)
public ResponseEntity<Map<String, Object>> userFriendRequest(@RequestBody UserFriendRequest userFriendRequest) {
	return this.friendsService.addUserFriends(userFriendRequest);
}

@RequestMapping(value = "/connectionsAtK", method = RequestMethod.POST)
public ResponseEntity<Map<String, Object>> findConnections(@RequestBody ConnectionRequestK connectionRequestK) {
	return this.friendsService.findConnections(connectionRequestK);
}


	
@RequestMapping(value = "/deleteFromFriendList", method = RequestMethod.POST)
public ResponseEntity<Map<String, Object>> deleteFromFriendList(@RequestBody UserFriendDelete userFriendDelete) {
	return this.friendsService.deleteFromFriendList(userFriendDelete);

}
	@RequestMapping(value = "/getFriendList", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> getUserFriendList(@RequestBody UserFriendListRequest userFriendListRequest) {
		return this.friendsService.getUserFriendsList(userFriendListRequest);
	
}

}
